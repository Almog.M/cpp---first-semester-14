#include "Boolean.h"

Boolean::Boolean(string strValue)
{
	if (Helper::isBoolean(strValue))
	{
		if (strValue.find('T') != -1) _value = true;
		else _value = false;
	}
	else
		_value = false;
	
}

string Boolean::toString() const
{
	if (this->_value == 1) return "True";
	else return "False";
	return string();
}
