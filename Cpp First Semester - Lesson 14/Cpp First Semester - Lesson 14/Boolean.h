#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Helper.h"

using std::string;

class Boolean : public Type
{
private:
	bool _value;

public:
	Boolean(bool value) { this->_value = value;  };
	Boolean(string strValue);
	virtual ~Boolean() {};
	virtual bool isPrintable() const { return true; };
	virtual string toString() const;
};
#endif // BOOLEAN_H