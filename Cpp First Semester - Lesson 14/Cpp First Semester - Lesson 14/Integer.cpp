#include "Integer.h"

Integer::Integer(string str)
{
	Helper::trim(str);
	if (Helper::isInteger(str))
	{
		_value = std::stoi(str);
	}
	else
		_value = -1;//Default for nothing...
}
