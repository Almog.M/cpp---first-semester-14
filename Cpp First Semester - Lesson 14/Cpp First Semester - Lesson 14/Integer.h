#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Helper.h"

using std::string;

class Integer : public Type
{
private:
	int _value;

public:
	Integer(string str);
	Integer(int value) { this->_value = value; };
	virtual ~Integer() {};
	virtual bool isPrintable() const { return true; };
	virtual string toString() const { return std::to_string(_value); };
};

#endif // INTEGER_H