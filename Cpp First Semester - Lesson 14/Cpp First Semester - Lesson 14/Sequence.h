#ifndef SEQUENCE_H
#define SEQUENCE_H


#include "type.h"
#include <string>

using std::string;

class Sequence : public Type
{
private:
	Type* _value;

public:
	Sequence() { this->_value = nullptr; };
	Sequence(Type* value) { this->_value = value; };
	virtual ~Sequence() {};
	virtual bool isPrintable() const { return false; };
	virtual string toString() const { return ""; };
};

#endif // SEQUENCE_H