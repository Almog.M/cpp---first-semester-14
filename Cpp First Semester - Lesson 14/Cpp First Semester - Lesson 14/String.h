#ifndef STRING_H
#define STRING_H


#include "Sequence.h"
#include <string>

using std::string;

class String : public Sequence
{
private:
	string _value;

public:
	String(string value) { this->_value = value; };
	virtual ~String() {};
	virtual bool isPrintable() const { return true; };
	virtual string toString() const { return _value; };
};


#endif // STRING_H