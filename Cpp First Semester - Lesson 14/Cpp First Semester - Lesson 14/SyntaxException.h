#ifndef SYNTAX_EXCEPTION_H
#define SYNTAX_EXCEPTION_H

class SyntaxException
{
public:
	const char* what();
};
#endif // SYNTAX_EXCEPTION_H