#include "type.h"

Type::Type()
{
	_isTemp = false;
}

Type::~Type()
{
}

bool Type::IsTempType() const
{
	return this->_isTemp;
}

void Type::setIsTempType(bool isTemp)
{
	this->_isTemp = isTemp;
}
