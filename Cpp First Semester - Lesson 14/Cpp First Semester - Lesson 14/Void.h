#ifndef VOID_H
#define VOID_H

#include "type.h"
#include <string>

using std::string;

class Void : public Type
{
private:
	void* _value;

public:
	Void(void* value) { this->_value = value; };
	virtual ~Void() {};
	virtual bool isPrintable() const { return false; };
	virtual string toString() const { return ""; };


};

#endif // VOID_H