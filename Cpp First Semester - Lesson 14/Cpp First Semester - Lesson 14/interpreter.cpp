#include "type.h"
#include "InterperterException.h"
#include "parser.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "IndentationException.h"
#include "SyntaxException.h"

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "[The King James - Almog Maman (lol)...]"


int main(int argc,char **argv)
{
	Type* type = nullptr;
	std::string valueAsString = "";
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	

	while (input_string != "quit()")
	{
		/* prasing command
		try 
		{
			Parser::parseString(input_string);
		}
		catch (IndentationException& e)
		{
			std::cout << e.what() << "\n";
		}*/

		try
		{
			type = Parser::parseString(input_string);
			if (type != NULL)
			{
				if (type->isPrintable())
				{
					valueAsString = type->toString();
					std::cout << "<value as string = " << valueAsString << ">\n";
					if (type->IsTempType()) delete type;
				}
			}
		}
		catch (SyntaxException& e)
		{
			std::cout << e.what() << "\n";
		}
		
		
		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	std::cout << "Good Bye , Do not forget to Play... (:\n";
	system("Pause");

	return 0;
}


