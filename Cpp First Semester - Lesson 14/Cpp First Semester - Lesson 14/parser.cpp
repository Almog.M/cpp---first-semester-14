#include "parser.h"
#include <iostream>
#include "Helper.h"
#include <string>
#include "IndentationException.h"
#include "type.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "Void.h"
#include "Sequence.h"
#include "SyntaxException.h"

Type* Parser::parseString(std::string str) 
{
	if (str.length() > 0)
	{
		Helper::trim(str);
		Type* type = Parser::getType(str);
		if (type != nullptr) return type;
		else throw(SyntaxException());
		//if (str.find('\t') == 0 || str.find(' ') == 0) //If the string starts with an indentation.
		//	throw IndentationException();
		//std::cout << str << std::endl;
	}

	return NULL;
}

Type * Parser::getType(std::string & str)
{
	Type* type = nullptr; //Type of the value in the string...
	Helper::trim(str);
	if (Helper::isBoolean(str)) type = new Boolean(str);
	else if (Helper::isInteger(str)) type = new Integer(str);
	else if (Helper::isString(str)) type = new String(str);
	
	if (type != nullptr) type->setIsTempType(true);
	return type;
}


