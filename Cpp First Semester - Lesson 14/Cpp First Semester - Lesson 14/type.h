#ifndef TYPE_H
#define TYPE_H

#include <string>
using std::string;

class Type
{
private:
	bool _isTemp;
public:
	Type();
	virtual ~Type();
	virtual bool isPrintable() const = 0;
	virtual string toString() const = 0;

	//getters:
	bool IsTempType() const;

	//Setters:
	void setIsTempType(bool isTemp);
};





#endif //TYPE_H
